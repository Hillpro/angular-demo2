import { Injectable } from '@angular/core';
import { Item } from '../models/item.model';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  private _items: Item[];
  private idIncrement: number;

  constructor() {
    this.idIncrement = +localStorage.getItem('AI') || 0;
    this._items = JSON.parse(localStorage.getItem('ITEMS')) || [];
  }

  get items(): Item[] {
    return this._items;
  }

  get(id: number): Item {
    return this._items.find(i => i.id === id);
  }

  create(name: string, description: string) {
    this.idIncrement++;
    const newItem = new Item(this.idIncrement, name, description);
    this._items.push(newItem);

    localStorage.setItem('ITEMS', JSON.stringify(this._items));
    localStorage.setItem('AI', JSON.stringify(this.idIncrement));

    console.log(this._items);
  }

  delete(item: Item) {
    this._items = this._items.filter(i => i.id !== item.id);

    localStorage.setItem('ITEMS', JSON.stringify(this._items));
  }
}
