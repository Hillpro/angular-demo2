import { Component, OnInit, Input } from '@angular/core';
import { Item } from 'src/app/models/item.model';
import { ItemsService } from 'src/app/services/items.service';
import { Router } from '@angular/router';

@Component({
  selector: '[app-item-row]',
  templateUrl: './item-row.component.html',
  styleUrls: ['./item-row.component.css']
})
export class ItemRowComponent implements OnInit {

  @Input() item: Item;

  constructor(private itemsService: ItemsService, private router: Router) { }

  ngOnInit() {
  }

  view() {
    this.router.navigate(['/items/', this.item.id]);
  }

  delete() {
    this.itemsService.delete(this.item);
  }
}
