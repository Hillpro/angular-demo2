import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, NgForm } from '@angular/forms';
import { ItemsService } from 'src/app/services/items.service';

@Component({
  selector: 'app-item-create',
  templateUrl: './item-create.component.html',
  styleUrls: ['./item-create.component.css']
})
export class ItemCreateComponent implements OnInit {

  @ViewChild('form', {static: true}) formElement: NgForm;

  itemForm: FormGroup;
  nameControl: FormControl;

  constructor(formBuilder: FormBuilder, private itemsService: ItemsService) {
    this.nameControl = new FormControl('', Validators.required);
    this.itemForm = formBuilder.group(
        {
          name: this.nameControl,
          description: new FormControl('', [Validators.required])
        }
    );

   }

  ngOnInit() {
  }

  create() {
    if (this.itemForm.valid) {
      const itemsValues = this.itemForm.value;
      this.itemsService.create(itemsValues.name, itemsValues.description);

      this.formElement.resetForm();
    }
  }
}
