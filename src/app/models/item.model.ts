export class Item {
  id: number;
  name: string;
  description: string;

  constructor(id: number, name: string, description: string) {
    this.name = name;
    this.description = description;
    this.id = id;
  }
}
